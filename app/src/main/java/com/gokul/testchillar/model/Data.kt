package com.gokul.testchillar.model

import com.squareup.moshi.Json

data class Data (
  @Json(name = "username") var username:String,
  @Json(name = "password") var password:String,
  @Json(name = "name")     var name:String,
  @Json(name = "place")    var place:String,
  @Json(name = "dob")      var dob:String
)

data class LoginResponse (
  @Json(name = "status") var status:Boolean,
  @Json(name = "data")   var data:Data
)