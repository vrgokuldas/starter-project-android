package com.gokul.testchillar.viewmodel

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.gokul.testchillar.model.LoginResponse
import com.gokul.testchillar.repo.NetworkRepository

class InitialScreenViewModel:ViewModel() {

    val repository = NetworkRepository()

    var signInResponse:MutableLiveData<LoginResponse> = MutableLiveData()

    fun getSingedIn(owner:LifecycleOwner)
    {
        repository.getSignedIn().observe(owner, Observer {
            signInResponse.value=it
        })
    }

    override fun onCleared() {
        super.onCleared()
        repository.job.cancel()
    }
}