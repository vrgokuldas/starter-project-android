package com.gokul.testchillar.network

import com.gokul.testchillar.model.LoginResponse
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import java.util.concurrent.TimeUnit

interface RestApiService {

    companion object
    {
        fun createCoreService(): RestApiService {

            val okHttpClient= OkHttpClient.Builder()
                .connectTimeout(1,TimeUnit.MINUTES)
                .readTimeout(30,TimeUnit.SECONDS)
                .writeTimeout(15,TimeUnit.SECONDS)
                .build()

            return Retrofit.Builder()
                .baseUrl(Url.BaseUrl)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .client(okHttpClient)
                .build().create(RestApiService::class.java)
        }
    }

    @GET(Url.LoginUrl)
    fun getLoginDetails():Deferred<LoginResponse>
}