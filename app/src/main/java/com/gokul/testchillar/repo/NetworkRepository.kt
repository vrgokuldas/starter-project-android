package com.gokul.testchillar.repo

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.gokul.testchillar.model.LoginResponse
import com.gokul.testchillar.network.RestApiService
import kotlinx.coroutines.*

class NetworkRepository {

    private val apiService by lazy { RestApiService.createCoreService() }
    val job=Job()
    private val coroutineScope= CoroutineScope(Dispatchers.IO + job)

    private var loginResponse = MutableLiveData<LoginResponse>()

    fun getSignedIn(): MutableLiveData<LoginResponse> {

        coroutineScope.launch{
            val request = apiService.getLoginDetails()
            withContext(Dispatchers.Main)
            {
                try {
                    val response = request.await()
                    loginResponse.value = response
                    Log.e("Response String",response.toString())
                }
                catch (e: Throwable)
                {
                    Log.e("LoginError", e.printStackTrace().toString())
                }
            }
        }
        return loginResponse
    }
}