package com.gokul.testchillar.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.gokul.testchillar.R
import com.gokul.testchillar.viewmodel.InitialScreenViewModel
import kotlinx.android.synthetic.main.activity_main.*

class InitialActivity : AppCompatActivity() {

    private lateinit var viewModel:InitialScreenViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel= ViewModelProviders.of(this).get(InitialScreenViewModel::class.java)

        viewModel.signInResponse.observe(this, Observer {
            Log.i("LoginResponse",it.toString())
            textView.text=it.toString()
        })
        button.setOnClickListener {
            viewModel.getSingedIn(this)
        }
    }
}
